# WooCommerce: Checkout Disclaimer
Mini-Extension to WooCommerce which adds a disclaimer to the checkout page.

## Requirements
To run WooCommerce: Checkout Disclaimer you need to have:
* Wordpress (at least 3.8, but it has been so far tested only on 4.4)
* WooCommerce (tested on 2.5.5)

## Install 
Upload the zip file from **release** directory, or different source to your Wordpress Plugins using the standard form at _/wp-admin/plugin-install.php?tab=upload_

![Wordpress Manual Plugin Install][01]

Don't forget to activate the newly installed plugin.

## Usage
No special requirements.

## Report Bugs
Please, [report all the bugs!](https://bitbucket.org/aidik/woocommerce-composite-products-clear-all-selections/issues)

## Contribution
Locally clone this repository and run:
	`npm install`

To create a release run:
	`grunt release`

Another useful **Grunt** task is `watch`.

## License

[MIT License](https://ixsystems.com) © Václav Navrátil

[01]: https://bytebucket.org/aidik/woocommerce-composite-products-freenas-storage-capacity/raw/400eeb940fe2b6e61db2ba2fbeb6e8257f3b5bec/doc/img/01.png?token=810c18c3d9292071cf4744382adbc56ef9ade228 "Wordpress Manual Plugin Install"
