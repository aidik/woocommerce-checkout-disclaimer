var path = require('path');

module.exports = function(grunt) {

	grunt.initConfig({

		// Import package manifest
		pkg: grunt.file.readJSON("package.json"),

		// Concat
		concat: {
			php: {
				src: ["src/banners/php.ban", "src/php/wc-checkout-disclaimer.php"],
				dest: "plugin/wc-checkout-disclaimer.php"
			}
		},

		// Watch for changes
		watch: {
			files: ["src/php/*", "src/banners/*.meta"],
			tasks: ["default"]
		},

		// Update banners
		makeBanners: {
		    phpBanner: {
				src: "src/banners/php.meta",
				dest: "src/banners/php.ban"
		    },
		    name: "<%= pkg.nice_name %>",
		    description: "<%= pkg.description %>",
		    version: "<%= pkg.version %>",
		    bugs: "<%= pkg.bugs.url %>",
		    author: "<%= pkg.author %>",
		    homepage: "<%= pkg.homepage %>",
		    license: "<%= pkg.license %>",
		    license_url: "<%= pkg.license_url %>",
		    repository: "<%= pkg.repository.url %>"
		},

		// Zips the plugin folder to make a release
		compress: {
			main: {
				options: {
					archive: 'release/<%= pkg.name %>-<%= pkg.version %>.zip'
    			},
    			files: [
      				{expand: true, cwd: 'plugin/', src: ['**/*'], dest: '<%= pkg.name %>/'}, // includes files in path and its subdirs
		    	]
  			}
		},

		// Renders Readme.md to a PDF file
		markdownpdf: {
    		options: {
    			paperBorder: "2.5cm",
    			cssPath: "doc/css/pdf-style.css"
          	},
    		files: {
      			src: "readme.md",
      			dest: "plugin/"
    		}
  		}
	});

	grunt.registerTask( "makeBanners", "Prepares banners.", function() {
		var now = new Date().toISOString();
		var config = grunt.config.get("makeBanners");
		var phpContent = grunt.file.read(config.phpBanner.src);
		grunt.file.write(config.phpBanner.dest, replace(phpContent, now));


		function replace(content, now) {
			content = content.replace("$$$author_email$$$", config.author.email);
			content = content.replace("$$$author_name$$$", config.author.name);
			content = content.replace("$$$author_url$$$", config.author.url);
			content = content.replace("$$$bugs$$$", config.bugs);
			content = content.replace("$$$description$$$", config.description);
			content = content.replace("$$$homepage$$$", config.homepage);
			content = content.replace("$$$license_name$$$", config.license);
			content = content.replace("$$$license_url$$$", config.license_url);
			content = content.replace("$$$name$$$", config.name);
			content = content.replace("$$$now$$$", now);
			content = content.replace("$$$repository$$$", config.repository);
			content = content.replace("$$$version$$$", config.version);

			return content;
		}

		console.log("makeBanners finished without errors.");

    });

	grunt.loadNpmTasks("grunt-contrib-compress");
	grunt.loadNpmTasks("grunt-contrib-concat");
	grunt.loadNpmTasks("grunt-contrib-watch");
	grunt.loadNpmTasks('grunt-markdown-pdf');

	grunt.registerTask("build", ["makeBanners", "concat"]);
	//grunt.registerTask("release", ["default", "markdownpdf", "compress"]);
	grunt.registerTask("release", ["default", "compress"]);
	grunt.registerTask("default", ["build"]);
};
