<?php

//Administration part
add_filter( 'plugin_row_meta', 'disclaimer_plugin_row_links', 10 ,2 );

function disclaimer_plugin_row_links( $links, $file ) {
	if ( $file == plugin_basename( __FILE__ ) ) {
		$new_links = array(
					'<a href="https://bitbucket.org/aidik/woocommerce-checkout-disclaimer/issues/" target="_blank">Report Bugs</a>',
					'<a href="'. plugin_dir_url( __FILE__ ) .'readme.pdf" target="_blank">Read Me</a>'
				);

		$links = array_merge( $links, $new_links );
	}

	return $links;
}



//Add iX disclaimer
add_action( 'woocommerce_checkout_order_review', 'add_ix_disclaimer', 15 );

function add_ix_disclaimer() {

    echo ('<div class="disclaimer"><p>Orders with shipment destinations outside of the United States require duties to be paid in country. The purchaser is responsible for those taxes, tariffs and or duties prior to customs clearance in country.</p><p>Each FreeNAS Mini product is built to order and goes through an extensive burn-in and testing process. Currently, lead time is approximately 1-2 weeks.</p></div>');
}

?>